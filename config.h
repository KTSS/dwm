/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 2;  /* border pixel of windows */
static const unsigned int snap      = 32; /* snap pixel */
static const unsigned int gappih    = 8;  /* horiz inner gap between windows */
static const unsigned int gappiv    = 8;  /* vert inner gap between windows */
static const unsigned int gappoh    = 8;  /* horiz outer gap between windows and screen edge */
static const unsigned int gappov    = 8;  /* vert outer gap between windows and screen edge */
static const int swallowfloating    = 0;  /* 1 means swallow floating windows by default */
static const int smartgaps          = 1;  /* 1 means no outer gap when there is only one window */
static const int showbar            = 1;  /* 0 means no bar */
static const int topbar             = 1;  /* 0 means bottom bar */
static const int vertpad            = 0;  /* vertical padding of bar */
static const int sidepad            = 0;  /* horizontal padding of bar */
static const char *fonts[]          = { "monospace:size=13" };
static char dmenufont[]             = "monospace:size=13";

/*
 * normbgcolor[]     Background for tags and blocks in statusbar
 * normbordercolor[] Border color of unfocused windows
 * normfgcolor[]     Letter color for tags and blocks in status bar
 * selbgcolor[]      Background for window title in statusbar
 * selbordercolor[]  Border color of focused windows
 * selfgcolor[]      Letter color for windows in status bar
 */

/* Color - dracula */
/* static char normbgcolor[]      = "#BD93F9"; */
/* static char selbgcolor[]       = "#CAA9FA"; */
/* static char normbordercolor[]  = "#000000"; */
/* static char selbordercolor[]   = "#FF79C6"; */
/* static char normfgcolor[]      = "#BBBBBB"; */
/* static char selfgcolor[]       = "#FFFFFF"; */

/* Color - gruvbox dark */
// static char normbgcolor[]      = "#1D2021";
// static char selbgcolor[]       = "#282828";
// static char normbordercolor[]  = "#32302F";
// static char selbordercolor[]   = "#F9F5D7";
// static char normfgcolor[]      = "#FBF1C7";
// static char selfgcolor[]       = "#F2E5BC";

/* Color - gruvbox light */
// static char normbgcolor[]      = "#F9F5D7";
// static char selbgcolor[]       = "#FBF1C7";
// static char normbordercolor[]  = "#F2E5BC";
// static char selbordercolor[]   = "#1D2021";
// static char normfgcolor[]      = "#282828";
// static char selfgcolor[]       = "#9D0006";

/* Color - nord */
// static char normbgcolor[]      = "#3B4252";
// static char selbgcolor[]       = "#4C566A";
// static char normbordercolor[]  = "#3B4252";
// static char selbordercolor[]   = "#4C566A";
// static char normfgcolor[]      = "#E5E9F0";
// static char selfgcolor[]       = "#ECEFF4";

/* Color - solarized dark */
// static char normbgcolor[]      = "#073642"; /* */
// static char selbgcolor[]       = "#073642"; /* */
// static char normbordercolor[]  = "#002B36"; /* */
// static char selbordercolor[]   = "#EEE8D5"; /* */
// static char normfgcolor[]      = "#FDF6E3"; /* */
// static char selfgcolor[]       = "#FDF6E3"; /* */

/* Color - solarized light */
/* static char normbgcolor[]      = "#EEE8D5"; */
/* static char selbgcolor[]       = "#EEE8D5"; */
/* static char normbordercolor[]  = "#FDF6E3"; */
/* static char selbordercolor[]   = "#073642"; */
/* static char normfgcolor[]      = "#002B36"; */
/* static char selfgcolor[]       = "#002B36"; */

/* Color - xterm */
// static char normbgcolor[]      = "#000000";
// static char selbgcolor[]       = "#000000";
// static char normbordercolor[]  = "#666666";
// static char selbordercolor[]   = "#FFFFFF";
// static char normfgcolor[]      = "#00FF00";
// static char selfgcolor[]       = "#FF0000";

/* Color - SS */
static char normbgcolor[]      = "#001818";
static char selbgcolor[]       = "#001818";
static char normbordercolor[]  = "#001818";
static char selbordercolor[]   = "#666666";
static char normfgcolor[]      = "#666666";
static char selfgcolor[]       = "#666666";

static char *colors[][3] = {
    /*               fg           bg           border         */
    [SchemeNorm] = { normfgcolor, normbgcolor, normbordercolor },
    [SchemeSel]  = { selfgcolor,  selbgcolor,  selbordercolor  },
};

typedef struct {
    const char *name;
    const void *cmd;
} Sp;
const char *spcmd1[] = {"alacritty", "--class", "spterm", "-o", "window.dimensions.columns=120", "-o", "window.dimensions.lines=34", NULL };
const char *spcmd2[] = {"alacritty", "--class", "spcalc", "-o", "window.dimensions.columns=50",  "-o", "window.dimensions.lines=20" "-e", "bc", "-lq", NULL };
const char *spcmd3[] = {"alacritty", "--class", "spfzwr", "-o", "window.dimensions.columns=50",  "-o", "window.dimensions.lines=20" "-e", "sk", "-c", "compgen", "-c", "--bind", "'enter:execute-silent({}&)+abort'", NULL };
static Sp scratchpads[] = {
    /* name    cmd  */
    {"spterm", spcmd1},
    {"spcalc", spcmd2},
    {"spfzwr", spcmd3},
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };
/*  Tags:
    1.  Communication
    2.  Terminal
    3. ﰍ Web
    4.  File Manager
    5.  Work/Notes
    6.  Art
    7.  Mail/Calendar
    8.  Music
    9.  Others
*/

static const Rule rules[] = {
    /* class                instance   title             tags mask     switchtotag,  isfloating   isterminal noswallow monitor */
    { "Signal",             NULL,      NULL,             1 << 0,       1,            0,           0,         0,        -1 },
    { "TelegramDesktop",    NULL,      NULL,             1 << 0,       1,            0,           0,         0,        -1 },
    { "St",                 NULL,      NULL,             1 << 1,       1,            0,           1,         0,        -1 },
    { "Alacritty",          NULL,      NULL,             1 << 1,       1,            0,           1,         0,        -1 },
    { "Konsole",            NULL,      NULL,             1 << 1,       1,            0,           1,         0,        -1 },
    { "Brave-browser",      NULL,      NULL,             1 << 2,       1,            0,           0,         0,        -1 },
    { "firefox",            NULL,      NULL,             1 << 2,       1,            0,           0,         0,        -1 },
    { "icecat-default",     NULL,      NULL,             1 << 2,       1,            0,           0,         0,        -1 },
    { "librewolf-default",  NULL,      NULL,             1 << 2,       1,            0,           0,         0,        -1 },
    { "dolphin",            NULL,      NULL,             1 << 3,       1,            0,           1,         0,        -1 },
    { "lf",                 NULL,      NULL,             1 << 3,       1,            0,           1,         0,        -1 },
    { "lfub",               NULL,      NULL,             1 << 3,       1,            0,           1,         0,        -1 },
    { "vifm",               NULL,      NULL,             1 << 3,       1,            0,           1,         0,        -1 },
    { "organization",       NULL,      NULL,             1 << 4,       1,            0,           1,         0,        -1 },
    { "Blender",            NULL,      NULL,             1 << 5,       1,            0,           0,         0,        -1 },
    { "Gimp",               NULL,      NULL,             1 << 5,       1,            0,           0,         0,        -1 },
    { "krita",              NULL,      NULL,             1 << 5,       1,            0,           0,         0,        -1 },
    { "neomutt",            NULL,      NULL,             1 << 6,       1,            0,           1,         0,        -1 },
    { "thunderbird",        NULL,      NULL,             1 << 6,       1,            0,           0,         0,        -1 },
    { "vlc",                NULL,      NULL,             1 << 7,       1,            0,           1,         0,        -1 },
    { "elisa",              NULL,      NULL,             1 << 7,       1,            0,           1,         0,        -1 },
    { "ncmpcpp",            NULL,      NULL,             1 << 7,       1,            0,           1,         0,        -1 },
    { "KeePassXC",          NULL,      NULL,             1 << 8,       1,            0,           0,         0,        -1 },
    { "tremc",              NULL,      NULL,             1 << 8,       1,            0,           1,         0,        -1 },
    { NULL,                 NULL,      "Event Tester",   0,            0,            0,           0,         1,        -1 },
    { NULL,                 "spterm",  NULL,             SPTAG(0),     0,            1,           1,         0,        -1 },
    { NULL,                 "spfzwr",  NULL,             SPTAG(0),     0,            1,           1,         0,        -1 },
    { NULL,                 "spcalc",  NULL,             SPTAG(1),     0,            1,           1,         0,        -1 },
};

/* layouts */
static const float mfact       = 0.60; /* factor of master area size [0.05..0.95] */
static const int   nmaster     = 1;    /* number of clients in master area */
static const int   resizehints = 1;    /* 1 means respect size hints in tiled resizals */
#include "gaplessgrid.c"
#define FORCE_VSPLIT 1  /* nrowgrid layout: force two clients to always split vertically */
#include "vanitygaps.c"
static const Layout layouts[] = {
    /* symbol  arrange function */
    { "[]=",   tile },                   // 1st=Default: Master on left, others on right
    { "TTT",   bstack },                 // Master on top, others on bottom
    //{ "[@]", spiral },                 // Fibonacci spiral */  (Instead of Fibonacci, gapless in the line below)
    { "###",   gaplessgrid },            // Gapless grid, for balance when opening 2^x ammounts of windows
    { "[\\]",  dwindle },                // Decreasing in size right and leftward
    { "H[]",   deck },                   // Master on left, others in monocle-like mode on right
    { "[M]",   monocle },                // All windows on top of eachother
    { "|M|",   centeredmaster },         // Master in middle, others on sides
    { ">M>",   centeredfloatingmaster }, // Same but master floats
    { "><>",   NULL },                   // no layout function means floating behavior
    { NULL,    NULL },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
    { MODKEY,                       KEY, view,       {.ui = 1 << TAG} }, \
    { MODKEY|ControlMask,           KEY, toggleview, {.ui = 1 << TAG} }, \
    { MODKEY|ShiftMask,             KEY, tag,        {.ui = 1 << TAG} }, \
    { MODKEY|ControlMask|ShiftMask, KEY, toggletag,  {.ui = 1 << TAG} },
#define STACKKEYS(MOD,ACTION) \
  {  MOD, XK_j, ACTION##stack, {.i = INC(+1) } }, \
  {  MOD, XK_k, ACTION##stack, {.i = INC(-1) } }, \
  {  MOD, XK_v, ACTION##stack, {.i = 0       } }, \

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static const char *termcmd[]  = { "alacritty", NULL };

#include <X11/XF86keysym.h>
#include "shiftview.c"
static Key keys[] = {
    STACKKEYS(MODKEY,           focus)
    STACKKEYS(MODKEY|ShiftMask, push)
    TAGKEYS( XK_1, 0)
    TAGKEYS( XK_2, 1)
    TAGKEYS( XK_3, 2)
    TAGKEYS( XK_4, 3)
    TAGKEYS( XK_5, 4)
    TAGKEYS( XK_6, 5)
    TAGKEYS( XK_7, 6)
    TAGKEYS( XK_8, 7)
    TAGKEYS( XK_9, 8)
    /* modifier                     key                       function        argument */
    { MODKEY,                       XK_0,                     view,           {.ui = ~0 } },
    { MODKEY|ShiftMask,             XK_0,                     tag,            {.ui = ~0 } },
    { MODKEY,                       XK_space,                 zoom,           {0} },
    { MODKEY|ShiftMask,             XK_space,                 togglefloating, {0} },
    { MODKEY,                       XK_semicolon,             shiftview,      { .i = 1 } },
    { MODKEY|ShiftMask,             XK_semicolon,             shifttag,       { .i = 1 } },
    // { MODKEY,                       XK_F1,                    spawn,          SHCMD("maim --format=png --quality=5 --select                           ~/b/select-`date +%F__%T`.png") },
    // { MODKEY,                       XK_F2,                    spawn,          SHCMD("maim --format=png --quality=5 --window=`xdotool getactivewindow` ~/b/window-`date +%F__%T`.png") },
    // { MODKEY,                       XK_F3,                    spawn,          SHCMD("maim --format=png --quality=5                                    ~/b/fullsc-`date +%F__%T`.png") },
    // { MODKEY,                       XK_F4,                    spawn,          SHCMD("maim --format=png --quality=5 --select                           | xclip -selection clipboard -t image/png") },
    // { MODKEY,                       XK_F5,                    spawn,          SHCMD("maim --format=png --quality=5 --window=`xdotool getactivewindow` | xclip -selection clipboard -t image/png") },
    // { MODKEY,                       XK_F6,                    spawn,          SHCMD("maim --format=png --quality=5                                    | xclip -selection clipboard -t image/png") },
    { MODKEY,                       XK_F1,                    spawn,          SHCMD("flameshot gui -p ~/b/") },
    { MODKEY,                       XK_F2,                    spawn,          SHCMD("flameshot screen -p ~/b/") },
    { MODKEY,                       XK_F3,                    spawn,          SHCMD("flameshot full -p ~/b/") },
    { MODKEY,                       XK_F4,                    spawn,          SHCMD("flameshot gui -c") },
    { MODKEY,                       XK_F5,                    spawn,          SHCMD("flameshot screen -c") },
    { MODKEY,                       XK_F6,                    spawn,          SHCMD("flameshot full -c") },
    { MODKEY,                       XK_F7,                    spawn,          SHCMD("displayselect") },
    { MODKEY,                       XK_F8,                    spawn,          SHCMD("torwrap") },
    { MODKEY,                       XK_F9,                    spawn,          SHCMD("dmenumount") },
    { MODKEY,                       XK_F10,                   spawn,          SHCMD("dmenuumount") },
    { MODKEY,                       XK_F11,                   spawn,          SHCMD("td-toggle") },
    { MODKEY,                       XK_F12,                   spawn,          SHCMD("killall screenkey || screenkey &") },
    { MODKEY,                       XK_Insert,                spawn,          SHCMD("notify-send \" Clipboard contents:\" \"$(xclip -o -selection clipboard)\"") },
    // { MODKEY,                       XK_Delete,                spawn,          SHCMD("") },
    // { MODKEY,                       XK_Home,                  spawn,          SHCMD("") },
    // { MODKEY,                       XK_End,                   spawn,          SHCMD("") },
    { MODKEY,                       XK_Page_Down,             shiftview,      { .i = +1 } },
    { MODKEY,                       XK_Page_Up,               shiftview,      { .i = -1 } },
    { MODKEY|ShiftMask,             XK_Page_Down,             shifttag,       { .i = +1 } },
    { MODKEY|ShiftMask,             XK_Page_Up,               shifttag,       { .i = -1 } },
    { 0,                            XK_Print,                 spawn,          SHCMD("flameshot gui") },
    // { 0,                            XK_Print,                 spawn,          SHCMD("maimpick") },
    { ShiftMask,                    XK_Print,                 spawn,          SHCMD("maim ~/b/`date +%F__%T`.png") },
    // { MODKEY,                       XK_Print,                 spawn,          SHCMD("dmenurecord") },
    // { MODKEY|ShiftMask,             XK_Print,                 spawn,          SHCMD("dmenurecord kill") },
    // { MODKEY,                       XK_apostrophe,            togglescratch,  {.ui = 1} }, // calculator
    // { MODKEY|ShiftMask,             XK_apostrophe,            togglescratch,  {.ui = 0} }, // scratchpad terminal
    { MODKEY,                       XK_equal,                 spawn,          SHCMD("pamixer --allow-boost -i 5;  kill -44 $(pidof dwmblocks)") },
    { MODKEY|ShiftMask,             XK_equal,                 spawn,          SHCMD("pamixer --allow-boost -i 15; kill -44 $(pidof dwmblocks)") },
    { MODKEY,                       XK_minus,                 spawn,          SHCMD("pamixer --allow-boost -d 5;  kill -44 $(pidof dwmblocks)") },
    { MODKEY|ShiftMask,             XK_minus,                 spawn,          SHCMD("pamixer --allow-boost -d 15; kill -44 $(pidof dwmblocks)") },
    { MODKEY,                       XK_bracketleft,           spawn,          SHCMD("mpc seek -10") },
    { MODKEY,                       XK_bracketright,          spawn,          SHCMD("mpc seek +10") },
    { MODKEY|ShiftMask,             XK_bracketleft,           spawn,          SHCMD("mpc seek -60") },
    { MODKEY|ShiftMask,             XK_bracketright,          spawn,          SHCMD("mpc seek +60") },
    { MODKEY,                       XK_comma,                 spawn,          SHCMD("mpc prev") },
    { MODKEY|ShiftMask,             XK_comma,                 spawn,          SHCMD("mpc seek 0%") },
    { MODKEY,                       XK_period,                spawn,          SHCMD("mpc next") },
    { MODKEY|ShiftMask,             XK_period,                spawn,          SHCMD("mpc repeat") },
    { MODKEY,                       XK_BackSpace,             spawn,          SHCMD("sysact") },
    { MODKEY|ShiftMask,             XK_BackSpace,             spawn,          SHCMD("sysact") },
    { MODKEY,                       XK_Left,                  focusmon,       {.i = -1 } },
    { MODKEY|ShiftMask,             XK_Left,                  tagmon,         {.i = -1 } },
    { MODKEY,                       XK_Right,                 focusmon,       {.i = +1 } },
    { MODKEY|ShiftMask,             XK_Right,                 tagmon,         {.i = +1 } },
    { MODKEY,                       XK_Tab,                   view,           {0} },
    { MODKEY,                       XK_Return,                spawn,          {.v = termcmd } },
    { MODKEY|ShiftMask,             XK_Return,                spawn,          {.v = spcmd3 } },
    { MODKEY,                       XK_backslash,             view,           {0} },
    { MODKEY,                       XK_a,                     spawn,          SHCMD("alacritty -e alsamixer") },
    { MODKEY,                       XK_b,                     togglebar,      {0} },
    { MODKEY,                       XK_c,                     spawn,          SHCMD("signal-desktop") },
    { MODKEY,                       XK_d,                     spawn,          SHCMD("/usr/local/bin/dmenu_run --autoselect") },
    // { MODKEY,                       XK_d,                     spawn,          {.v = dmenucmd } },
    { MODKEY,                       XK_e,                     spawn,          SHCMD("alacritty --class neomutt -e neomutt ; pkill -RTMIN+12 dwmblocks") },
    { MODKEY,                       XK_f,                     togglefullscr,  {0} },
    { MODKEY,                       XK_g,                     shiftview,      { .i = -1 } },
    { MODKEY,                       XK_h,                     setmfact,       {.f = -0.05} },
    { MODKEY,                       XK_i,                     setlayout,      {.v = &layouts[6]} }, /* centeredmaster */
    // j is bounded above
    // k is bounded above
    { MODKEY,                       XK_l,                     setmfact,       {.f = +0.05} },
    { MODKEY,                       XK_m,                     spawn,          SHCMD("alacritty --class ncmpcpp -e ncmpcpp") },
    { MODKEY,                       XK_n,                     spawn,          SHCMD("neovide --multigrid --x11-wm-class organization /mnt/ssddata/neorg/personal/index.norg") },
    { MODKEY,                       XK_o,                     spawn,          SHCMD("alacritty --class organization -e nvim /mnt/ssddata/neorg/personal/index.norg") },
    { MODKEY,                       XK_p,                     spawn,          SHCMD("mpc toggle") },
    { MODKEY,                       XK_q,                     killclient,     {0} },
    { MODKEY,                       XK_r,                     spawn,          SHCMD("alacritty --class $FILE -e $FILE") },
    // { MODKEY,                       XK_s,                     spawn,          SHCMD("") },
    { MODKEY,                       XK_t,                     setlayout,      {.v = &layouts[0]} }, /* tile */
    { MODKEY,                       XK_u,                     setlayout,      {.v = &layouts[4]} }, /* deck */
    // v is bounded above
    { MODKEY,                       XK_w,                     spawn,          SHCMD("$BROWSER") },
    { MODKEY,                       XK_x,                     spawn,          SHCMD("keepassxc") },
    { MODKEY,                       XK_y,                     setlayout,      {.v = &layouts[2]} }, /* gapless */
    // { MODKEY,                       XK_z,                     spawn,          SHCMD("") },
    // { MODKEY|ShiftMask,             XK_a,                     spawn,          SHCMD("") },
    // { MODKEY|ShiftMask,             XK_b,                     spawn,          SHCMD("") },
    { MODKEY|ShiftMask,             XK_c,                     spawn,          SHCMD("telegram-desktop") },
    // { MODKEY|ShiftMask,             XK_d,                     spawn,          SHCMD("") },
    { MODKEY|ShiftMask,             XK_e,                     spawn,          SHCMD("alacritty --class neovim -e nvim") },
    // { MODKEY|ShiftMask,             XK_f,                     spawn,          SHCMD("") },
    { MODKEY|ShiftMask,             XK_g,                      shifttag,       { .i = -1 } },
    // { MODKEY|ShiftMask,             XK_h,                      spawn,          SHCMD("") },
    { MODKEY|ShiftMask,             XK_i,                      setlayout,      {.v = &layouts[7]} }, /* centeredfloatingmaster */
    // J is bounded above
    // K is bounded above
    // { MODKEY|ShiftMask,             XK_l,                      spawn,          SHCMD("") },
    { MODKEY|ShiftMask,             XK_m,                      spawn,          SHCMD("pamixer -t; kill -44 $(pidof dwmblocks)") },
    { MODKEY|ShiftMask,             XK_n,                      spawn,          SHCMD("alacritty --class newsboat -e newsboat; pkill -RTMIN+6 dwmblocks") },
    // { MODKEY|ShiftMask,             XK_o,                      spawn,          SHCMD("") },
    { MODKEY|ShiftMask,             XK_p,                      spawn,          SHCMD("mpc pause ; pauseallmpv") },
    { MODKEY|ShiftMask,             XK_q,                      spawn,          SHCMD("sysact") },
    { MODKEY|ShiftMask,             XK_r,                      setlayout,      {.v = &layouts[1]} }, /* bstack */
    // { MODKEY|ShiftMask,             XK_s,                      spawn,          SHCMD("") },
    { MODKEY|ShiftMask,             XK_t,                      spawn,          SHCMD("alacritty -e top") },
    { MODKEY|ShiftMask,             XK_u,                      setlayout,      {.v = &layouts[5]} }, /* monocle */
    // { MODKEY|ShiftMask,             XK_v,                      spawn,          SHCMD("") },
    { MODKEY|ShiftMask,             XK_w,                      spawn,          SHCMD("$BROWSER2") },
    // { MODKEY|ShiftMask,             XK_x,                      spawn,          SHCMD("") },
    { MODKEY|ShiftMask,             XK_y,                      setlayout,      {.v = &layouts[3]} }, /* dwindle */
    // { MODKEY|ShiftMask,             XK_z,                      spawn,          SHCMD("") },
    // Audio control
    { 0,                            XF86XK_AudioLowerVolume,  spawn,          SHCMD("lmc down;   kill -44 $(pidof dwmblocks)") },
    { 0,                            XF86XK_AudioRaiseVolume,  spawn,          SHCMD("lmc up;     kill -44 $(pidof dwmblocks)") },
    { 0,                            XF86XK_AudioMute,         spawn,          SHCMD("lmc toggle; kill -44 $(pidof dwmblocks)") },
    // { 0,                            XF86XK_AudioMedia,        spawn,          SHCMD("alacritty --class ncmpcpp -e ncmpcpp") },
    // { 0,                            XF86XK_AudioMicMute,      spawn,          SHCMD("pactl set-source-mute @DEFAULT_SOURCE@ toggle") },
    // Music control
    { 0,                            XF86XK_AudioForward,      spawn,          SHCMD("mpc seek +10") },
    { 0,                            XF86XK_AudioNext,         spawn,          SHCMD("mpc next") },
    { 0,                            XF86XK_AudioPause,        spawn,          SHCMD("mpc pause") },
    { 0,                            XF86XK_AudioPlay,         spawn,          SHCMD("mpc play") },
    { 0,                            XF86XK_AudioPrev,         spawn,          SHCMD("mpc prev") },
    { 0,                            XF86XK_AudioRewind,       spawn,          SHCMD("mpc seek -10") },
    { 0,                            XF86XK_AudioStop,         spawn,          SHCMD("mpc stop") },
    { 0,                            XF86XK_Calculator,        spawn,          SHCMD("alacritty --class calculator -e bc -l") },
    { 0,                            XF86XK_DOS,               spawn,          SHCMD("alacritty") },
    { 0,                            XF86XK_Launch1,           spawn,          SHCMD("xset dpms force off") },
    { 0,                            XF86XK_Mail,              spawn,          SHCMD("alacritty --class neomutt -e neomutt ; pkill -RTMIN+12 dwmblocks") },
    { 0,                            XF86XK_MonBrightnessDown, spawn,          SHCMD("xbacklight -dec 15") },
    { 0,                            XF86XK_MonBrightnessUp,   spawn,          SHCMD("xbacklight -inc 15") },
    { 0,                            XF86XK_MyComputer,        spawn,          SHCMD("alacritty -e $FILE /") },
    { 0,                            XF86XK_PowerOff,          spawn,          SHCMD("sysact") },
    { 0,                            XF86XK_ScreenSaver,       spawn,          SHCMD("slock & xset dpms force off; mpc pause; pauseallmpv") },
    { 0,                            XF86XK_Sleep,             spawn,          SHCMD("sudo -A zzz") },
    { 0,                            XF86XK_TaskPane,          spawn,          SHCMD("alacritty -e $SYSTEMMON") },
    { 0,                            XF86XK_TouchpadOff,       spawn,          SHCMD("synclient TouchpadOff=1") },
    { 0,                            XF86XK_TouchpadOn,        spawn,          SHCMD("synclient TouchpadOff=0") },
    { 0,                            XF86XK_TouchpadToggle,    spawn,          SHCMD("(synclient | grep 'TouchpadOff.*1' && synclient TouchpadOff=0) || synclient TouchpadOff=1") },
    { 0,                            XF86XK_WWW,               spawn,          SHCMD("$BROWSER") },
    /* J and K are automatically bound above in STACKEYS */
    /* V is automatically bound above in STACKKEYS */
    /* { 0,                             XF86XK_Battery,           spawn,          SHCMD("") }, */
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
    /* click         event mask button   function      argument */
#ifndef __OpenBSD__
    { ClkWinTitle,   0,         Button2, zoom,         {0} },
    { ClkStatusText, 0,         Button1, sigdwmblocks, {.i = 1}  },
    { ClkStatusText, 0,         Button2, sigdwmblocks, {.i = 2}  },
    { ClkStatusText, 0,         Button3, sigdwmblocks, {.i = 3}  },
    { ClkStatusText, 0,         Button4, sigdwmblocks, {.i = 4}  },
    { ClkStatusText, 0,         Button5, sigdwmblocks, {.i = 5}  },
    { ClkStatusText, ShiftMask, Button1, sigdwmblocks, {.i = 6}  },
#endif
    { ClkClientWin,  MODKEY,    Button1, movemouse,    {0}       },
    { ClkClientWin,  MODKEY,    Button2, defaultgaps,  {0}       },
    { ClkClientWin,  MODKEY,    Button3, resizemouse,  {0}       },
    { ClkClientWin,  MODKEY,    Button4, incrgaps,     {.i = +1} },
    { ClkClientWin,  MODKEY,    Button5, incrgaps,     {.i = -1} },
    { ClkRootWin,    0,         Button2, togglebar,    {0}       },
    { ClkStatusText, ShiftMask, Button3, spawn,        SHCMD("st -e nvim ~/.local/src/dwmblocks/config.h") },
    { ClkTagBar,     0,         Button1, view,         {0}       },
    { ClkTagBar,     0,         Button3, toggleview,   {0 }      },
    { ClkTagBar,     0,         Button4, shiftview,    {.i = -1} },
    { ClkTagBar,     0,         Button5, shiftview,    {.i = 1}  },
    { ClkTagBar,     MODKEY,    Button1, tag,          {0}       },
    { ClkTagBar,     MODKEY,    Button3, toggletag,    {0}       },
};
