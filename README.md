# Sennin's DWM

## FAQ

> What are the bindings?

Although the bindings can be easily discovered by reading the
[config.h](config.h) as per the suckless philosophy, check out the manual.
While originally based on Luke Smith's LARBS and Voidrice, `larbs.mom` does not
exist anymore.

> I want to it on systems other than Archlinux/Fedora/Voidlinux

Don't forget to install the prerequisites for compiling this build, and if
building on OpenBSD, don't forget to adjust the `config.mk` file to point to
the correct library location.
## Patches and features

- Clickable status bar based on Luke Smith's
  [dwmblocks](https://github.com/lukesmithxyz/dwmblocks).
- Other layouts: bstack, fibonacci, deck, centered master and more.
- True Fullscreen (`super+f`) and prevents focus shifting.
- Stacker: Move windows up the stack manually (`super-K/J`).
- Shiftview: Cycle through tags (`super+g/;`).
- Vanitygaps: Gaps allowed across all layouts (disabled by default to enjoy
  screen real state).
- Swallow patch: if a program run from a terminal would make it inoperable, it
  temporarily takes its place to save space.
- Nerd font: no need for so many colored emojis, keep things simple and pretty
